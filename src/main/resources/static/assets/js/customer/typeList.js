//一般直接写在一个js文件中
var layer, form, table;
layui.use(['layer', 'form', 'table'], function () {
    layer = layui.layer;
    form = layui.form;
    table = layui.table;
    table.render({
        elem: '#customerTypeTb',
        url: ctx + 'customerType/search',
        cols: [[
            {type: 'numbers', title: '序号'},
            {field: 'name', title: '类型名称'},
            {fixed: 'right', title: '操作', toolbar: '#customerTypeTbBar', width: 150, align: "center"}
        ]]
    });
    //监听行工具事件
    table.on('tool(customerTypeTb)', function (obj) {
        var data = obj.data;
        var id = data.id;
        if (obj.event === 'del') {
            layer.confirm('确认删除？', function (index) {
                obj.del();
                sys.ajax.doAjax({
                    type: 'POST',
                    url: ctx + 'customerType/del?id='+id,
                    dataType: 'json',
                    success: function (result) {
                        if (result.code == 0) {
                           // sysLayer.util.alert("删除成功");
                        } else {
                            sysLayer.util.alert(result.msg);
                        }
                        table.reload('customerTypeTb');
                    }
                });

                layer.close(index);
            });
        } else if (obj.event === 'edit') {
            layer.prompt({
                formType: 2,
                value: data.name,
                title: "修改类型"
            }, function (value, index) {
                obj.update({
                    name: value
                });
                sys.ajax.doAjax({
                    type: 'POST',
                    url: ctx + 'customerType/saveOrUpdate',
                    data: {"id":id,"name": value},
                    dataType: 'json',
                    success: function (result) {
                        if (result.code == 0) {
                            //sysLayer.util.alert("保存成功");
                        } else {
                            sysLayer.util.alert(result.msg);
                        }
                        table.reload('customerTypeTb');
                    }
                });
                layer.close(index);
            });
        }
    });
});
function add() {
    layer.prompt({
        formType: 2,
        title: "添加类型"
    }, function (value, index) {
        sys.ajax.doAjax({
            type: 'POST',
            url: ctx + 'customerType/saveOrUpdate',
            data: {"name": value},
            dataType: 'json',
            success: function (result) {
                if (result.code == 0) {
                    //sysLayer.util.alert("保存成功");
                } else {
                    sysLayer.util.alert(result.msg);
                }
                table.reload('customerTypeTb');
            }
        });
        layer.close(index);
    });
}