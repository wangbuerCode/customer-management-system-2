//一般直接写在一个js文件中
var layer, form, table;
layui.use(['layer', 'form', 'table'], function () {
    layer = layui.layer;
    form = layui.form;
    table = layui.table;
    search();
    //监听行工具事件
    table.on('tool(customerTb)', function (obj) {
        var data = obj.data;
        var id = data.id;
        if (obj.event === 'del') {
            layer.confirm('确认删除？', function (index) {
                obj.del();
                sys.ajax.doAjax({
                    type: 'POST',
                    url: ctx + 'customer/del?id=' + id,
                    dataType: 'json',
                    success: function (result) {
                        if (result.code == 0) {
                        } else {
                            sysLayer.util.alert(result.msg);
                        }
                        table.reload('customerTb');
                    }
                });
                layer.close(index);
            });
        } else if (obj.event === 'edit') {
            popBox(data)
        } else if (obj.event === 'recharge') {
            popRechargeBox(data)
        } else if (obj.event === 'consume') {
            popConsumePox(data)
        } else if (obj.event === 'bill') {
            popBillPox(data)
        }
    });
});
/**
 * 搜索
 */
function search() {
    table.render({
        elem: '#customerTb',
        url: ctx + 'customer/search',
        where: $("#searchForm").serializeJson(),
        cols: [[
            {type: 'numbers', title: '序号'},
            {field: 'name', title: '姓名'},
            {field: 'customerType', title: '客户类型', align: "center", width: 90},
            {
                field: 'sex', title: '性别', width: 60, align: "center",
                templet: function (d) {
                    return d.sex.name
                }
            },
            {field: 'mobile', title: '电话', width: 120, align: "center"},
            {field: 'cardNo', title: '卡号', width: 160},
            {field: 'money', title: '卡余额'},
            {field: 'address', title: '地址'},
            {field: 'createTime', title: '创建时间', width: 170, align: "center"},
            {fixed: 'right', title: '操作', toolbar: '#customerTbBar', width: 220, align: "center"}
        ]],
        request: {
            pageName: 'pageNo', //页码的参数名称，默认：page
            limitName: 'pageSize' //每页数据量的参数名，默认：limit
        },
        page: {
            theme: '#1E9FFF',
        },
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalCount, //解析数据长度
                "data": res.data.list //解析数据列表
            };
        }
    });
}
/**
 * 搜索账单
 */
function searchBill() {
    table.render({
        elem: '#billTb',
        url: ctx + 'cardRecord/search',
        where: $("#searchBillForm").serializeJson(),
        height: 'full-300', //高度最大化减去差值
        cols: [[
            {type: 'numbers', title: '序号'},
            {field: 'name', title: '姓名'},
            {field: 'mobile', title: '电话', width: 120, align: "center"},
            {field: 'cardNo', title: '卡号', width: 160},
            {field: 'businessType', title: '交易类型', align: "center",
                templet: function (d) {
                    if(d.businessType.code=='RECHARGE'){
                       return '<span  style="color: #FFB800">'+d.businessType.name+'</span>'
                    }else {
                       return  '<span style="color: #FF5722">'+d.businessType.name+'</span>'
                    }
                }},
            {field: 'changeNum', title: '交易金额'},
            {field: 'money', title: '卡余额'},
            {field: 'realMoney', title: '实收金额'},
            {field: 'couponMoney', title: '优惠金额'},
            {field: 'createTime', title: '创建时间', width: 170, align: "center"},
        ]],
        request: {
            pageName: 'pageNo', //页码的参数名称，默认：page
            limitName: 'pageSize' //每页数据量的参数名，默认：limit
        },
        page: {
            theme: '#1E9FFF',
        },
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalCount, //解析数据长度
                "data": res.data.list //解析数据列表
            };
        }
    });
}
//添加
function goAdd() {
    popBox();
}
/**
 * 添加、修改弹出框
 */
function popBox(data) {
    //移出禁用
    //$(".popBox select[name='departId']").attr("disabled", false);
    var title = '新增客户';
    var requestUrl = ctx + "customer/addOrUpdate";
    if (sys.util.isEmpty(data)) {//添加
        $("#customerInfoPoxForm input[name='id']").val("");
        $("#customerInfoPoxForm input[name='sex']:first").attr("checked", true);
    } else {//修改
        title = '修改客户';
        $("#customerInfoPoxForm input[name='id']").val(data.id);
        $("#customerInfoPoxForm input[name='name']").val(data.name);
        $("#customerInfoPoxForm input[name='sex']").each(function (i, item) {
            if ($(item).val() == data.sex.code) {
                $(item).prop('checked', true);
            }
        });
        $("#customerInfoPoxForm input[name='mobile']").val(data.mobile);
        $("#customerInfoPoxForm input[name='address']").val(data.address);
        $("#customerInfoPoxForm textarea[name='remark']").val(data.remark);
        $("#customerInfoPoxForm select[name='customerTypeId']").val(data.customerTypeId);
    }
    layui.form.render();//重新渲染表单
    // validatePopBoxForm();

    $(".customerPox").removeClass("layui-hide");
    var index = layer.open({
        title: title,
        area: ['600px', '620px'],
        btn: ['保存', '取消'],
        btnAlign: 'c',
        type: 1,
        content: $('.customerPox'),
        yes: function () {
            /*  if (!validatePopBoxForm().form()) {
             return;
             }*/
            var data = $("#customerInfoPoxForm").serialize2();
            sys.ajax.doAjax({
                type: 'post',
                url: requestUrl,
                data: data,
                dataType: 'json',
                success: function (result) {
                    if (result.code == 0) {
                        sysLayer.util.alert("保存成功", {
                            ok: function () {
                                layer.close(index);
                                table.reload('customerTb');
                            }
                        });
                    } else {
                        sysLayer.util.alert(result.msg);
                    }
                }
            });
        },
        end: function () {
            $("#customerInfoPoxForm")[0].reset();
            $(".customerPox").addClass("layui-hide");
            layer.close(index);
        }
    });
}
/**
 * 充值弹出框
 */
function popRechargeBox(data) {
    $("#customerRechargePoxForm input[name='customerId']").val(data.id);
    $("#customerRechargePoxForm input[name='cardId']").val(data.cardId);
    $("#customerRechargePoxForm input[name='name']").val(data.name);
    $("#customerRechargePoxForm input[name='mobile']").val(data.mobile);
    $("#customerRechargePoxForm input[name='money']").val(data.money);
    //layui.form.render();//重新渲染表单
    $(".customerRechargePox").removeClass("layui-hide");
    var index = layer.open({
        title: "充值窗口",
        area: ['600px', '620px'],
        btn: ['保存', '取消'],
        btnAlign: 'c',
        type: 1,
        content: $('.customerRechargePox'),
        yes: function () {
            var data = $("#customerRechargePoxForm").serialize2();
            sys.ajax.doAjax({
                type: 'post',
                url: ctx + "customer/recharge",
                data: data,
                dataType: 'json',
                success: function (result) {
                    if (result.code == 0) {
                        sysLayer.util.alert("保存成功", {
                            ok: function () {
                                layer.close(index);
                                table.reload('customerTb');
                            }
                        });
                    } else {
                        sysLayer.util.alert(result.msg);
                    }
                }
            });
        },
        end: function () {
            $("#customerRechargePoxForm")[0].reset();
            $(".customerRechargePox").addClass("layui-hide");
            layer.close(index);
        }
    });
}
/**
 * 消费弹出框
 */
function popConsumePox(data) {
    $("#customerConsumePoxForm input[name='customerId']").val(data.id);
    $("#customerConsumePoxForm input[name='cardId']").val(data.cardId);
    $("#customerConsumePoxForm input[name='name']").val(data.name);
    $("#customerConsumePoxForm input[name='mobile']").val(data.mobile);
    $("#customerConsumePoxForm input[name='money']").val(data.money);
    //layui.form.render();//重新渲染表单
    $(".customerConsumePox").removeClass("layui-hide");
    var index = layer.open({
        title: "消费窗口",
        area: ['600px', '620px'],
        btn: ['保存', '取消'],
        btnAlign: 'c',
        type: 1,
        content: $('.customerConsumePox'),
        yes: function () {
            var data = $("#customerConsumePoxForm").serialize2();
            sys.ajax.doAjax({
                type: 'post',
                url: ctx + "customer/consume",
                data: data,
                dataType: 'json',
                success: function (result) {
                    if (result.code == 0) {
                        sysLayer.util.alert("保存成功", {
                            ok: function () {
                                layer.close(index);
                                table.reload('customerTb');
                            }
                        });
                    } else {
                        sysLayer.util.alert(result.msg);
                    }
                }
            });
        },
        end: function () {
            $("#customerConsumePoxForm")[0].reset();
            $(".customerConsumePox").addClass("layui-hide");
            layer.close(index);
        }
    });
}
/**
 * 对账单弹出框
 */
function popBillPox(data) {
    $("#searchBillForm input[name='customerId']").val(data.id);
    searchBill();
    $(".billPox").removeClass("layui-hide");
    var index = layer.open({
        title: "【"+data.name+"】"+"对账单窗口",
        area: ['1200px', '700px'],
        btn: ['关闭'],
        btnAlign: 'c',
        type: 1,
        content: $('.billPox'),
        yes: function () {
            layer.close(index);
        },
        end: function () {
            $("#searchBillForm")[0].reset();
            $(".billPox").addClass("layui-hide");
            layer.close(index);
        }
    });
}
//限制两位小数
function limit2Float(obj) {
    obj.value = obj.value.replace(/[^\d.]/g, "");  //清除“数字”和“.”以外的字符
    obj.value = obj.value.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的
    obj.value = obj.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".");
    obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3');//只能输入两个小数
    if (obj.value.indexOf(".") < 0 && obj.value != "") {//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
        obj.value = parseFloat(obj.value);
    }
    if(obj.value<0){
        obj.value = 0;
    }
}
//计算充值金额
function calRechargeMoney() {
    var realMoney = parseFloat($("#customerRechargePoxForm input[name='realMoney']").val());
    var couponMoney = parseFloat($("#customerRechargePoxForm input[name='couponMoney']").val());
    var rechargeMoney = realMoney + couponMoney;
    rechargeMoney = rechargeMoney.toFixed(2);
    $("#customerRechargePoxForm input[name='rechargeMoney']").val(rechargeMoney);
}