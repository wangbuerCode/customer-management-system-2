//一般直接写在一个js文件中
var layer, form, table;
layui.use(['layer', 'form', 'table'], function () {
    layer = layui.layer;
    form = layui.form;
    table = layui.table;
    search();
});

function search() {
    table.render({
        elem: '#rechargeRecordTb',
        url: ctx + 'rechargeRecord/search',
        where: $("#searchForm").serializeJson(),
        cols: [[
            {type: 'numbers', title: '序号'},
            {field: 'name', title: '姓名'},
            {field: 'mobile', title: '电话', width: 120, align: "center"},
            {field: 'cardNo', title: '卡号', width: 160},
            {field: 'realMoney', title: '实收金额'},
            {field: 'couponMoney', title: '优惠金额'},
            {field: 'rechargeMoney', title: '储值金额'},
            {field: 'money', title: '卡余额'},
            {field: 'createTime', title: '创建时间', width: 170, align: "center"},
        ]],
        request: {
            pageName: 'pageNo', //页码的参数名称，默认：page
            limitName: 'pageSize' //每页数据量的参数名，默认：limit
        },
        page: {
            theme: '#1E9FFF',
        },
        parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.totalCount, //解析数据长度
                "data": res.data.list //解析数据列表
            };
        }
    });
}