package com.rain.dao.customer;

import com.rain.domain.entity.customer.CustomerType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 客户类型 Mapper 接口
 */
public interface CustomerTypeMapper extends BaseMapper<CustomerType> {

}
