package com.rain.dao.customer;

import com.rain.domain.entity.customer.Card;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 卡 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2018-12-01
 */
public interface CardMapper extends BaseMapper<Card> {

}
