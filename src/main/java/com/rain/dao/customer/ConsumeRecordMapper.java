package com.rain.dao.customer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rain.domain.dto.customer.ConsumeRecordSearchDto;
import com.rain.domain.entity.customer.ConsumeRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rain.domain.vo.customer.ConsumeRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 消费记录 Mapper 接口
 */
public interface ConsumeRecordMapper extends BaseMapper<ConsumeRecord> {
    /**
     * 消费记录搜索
     *
     * @param page
     * @param searchDto
     * @return
     */
    List<ConsumeRecordVo> search(@Param("page") Page<ConsumeRecordVo> page, @Param("searchDto") ConsumeRecordSearchDto searchDto);
}
