package com.rain.dao.customer;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rain.domain.dto.customer.CardRecordSearchDto;
import com.rain.domain.entity.customer.CardRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rain.domain.vo.customer.CardRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 卡记录 Mapper 接口
 */
public interface CardRecordMapper extends BaseMapper<CardRecord> {
    /**
     * 对账单记录搜索
     *
     * @param page
     * @param searchDto
     * @return
     */
    List<CardRecordVo> search(@Param("page") Page<CardRecordVo> page, @Param("searchDto") CardRecordSearchDto searchDto);
}
