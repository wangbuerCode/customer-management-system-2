package com.rain.dao.customer;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rain.domain.dto.customer.RechargeRecordSearchDto;
import com.rain.domain.entity.customer.RechargeRecord;
import com.rain.domain.vo.customer.RechargeRecordVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 充值记录 Mapper 接口
 */
public interface RechargeRecordMapper extends BaseMapper<RechargeRecord> {
    /**
     * 充值记录搜索
     *
     * @param page
     * @param searchDto
     * @return
     */
    List<RechargeRecordVo> search(@Param("page") Page<RechargeRecordVo> page, @Param("searchDto") RechargeRecordSearchDto searchDto);
}
