package com.rain.dao.customer;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rain.domain.dto.customer.CustomerSearchDto;
import com.rain.domain.entity.customer.Customer;
import com.rain.domain.vo.customer.CustomerVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 客户 Mapper 接口
 */
public interface CustomerMapper extends BaseMapper<Customer> {

    /**
     * 分页搜索
     *
     * @param page
     * @param searchDto
     */
    List<CustomerVo> search(@Param("page") Page<CustomerVo> page, @Param("searchDto") CustomerSearchDto searchDto);
}
