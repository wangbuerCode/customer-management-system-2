package com.rain.dao.user;

import com.rain.domain.entity.user.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper 接口
 */
public interface UserMapper extends BaseMapper<User> {

}
