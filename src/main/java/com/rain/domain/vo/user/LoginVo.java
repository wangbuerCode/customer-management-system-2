package com.rain.domain.vo.user;

import com.rain.domain.entity.user.User;

import java.io.Serializable;

/**
 * 用户登录信息
 */
public class LoginVo implements Serializable {
    private static final long serialVersionUID = 6214031630565144235L;
    /**
     * 登录用户
     */
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
