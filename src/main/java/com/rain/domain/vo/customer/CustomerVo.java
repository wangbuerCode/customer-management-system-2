package com.rain.domain.vo.customer;

import com.rain.domain.entity.customer.Customer;

import java.math.BigDecimal;

/**
 * 客户VO
 */
public class CustomerVo extends Customer {
    private static final long serialVersionUID = -7679483387814392937L;
    /**
     * 卡ID
     */
    private String cardId;

    /**
     * 卡号
     */
    private String cardNo;

    /**
     * 卡余额
     */
    private BigDecimal money;

    /**
     * 客户类型
     */
    private String customerType;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
}
