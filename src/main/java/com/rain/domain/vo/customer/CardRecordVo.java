package com.rain.domain.vo.customer;

import com.rain.domain.entity.customer.CardRecord;

import java.math.BigDecimal;

/**
 * 对账单记录VO
 */
public class CardRecordVo extends CardRecord {
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 卡号
     */
    private String cardNo;
    /**
     * 实收金额
     */
    private BigDecimal realMoney;
    /**
     * 优惠金额
     */
    private BigDecimal couponMoney;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(BigDecimal couponMoney) {
        this.couponMoney = couponMoney;
    }
}
