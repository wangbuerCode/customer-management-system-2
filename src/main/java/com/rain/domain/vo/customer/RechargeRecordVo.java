package com.rain.domain.vo.customer;

import com.rain.domain.entity.customer.RechargeRecord;

/**
 * 充值记录VO
 */
public class RechargeRecordVo extends RechargeRecord {
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String mobile;
    /**
     * 卡号
     */
    private String cardNo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
