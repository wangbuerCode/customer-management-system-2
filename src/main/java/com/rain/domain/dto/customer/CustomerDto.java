package com.rain.domain.dto.customer;

import com.rain.common.enums.SexEnum;

import java.io.Serializable;

/**
 * 客户保存DTO
 */
public class CustomerDto implements Serializable {
    private static final long serialVersionUID = -7679483387814392937L;
    /**
     * ID
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 类型ID
     */
    private String customerTypeId;

    /**
     * 性别
     */
    private SexEnum sex;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
