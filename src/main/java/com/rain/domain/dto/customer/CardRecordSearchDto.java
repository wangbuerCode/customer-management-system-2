package com.rain.domain.dto.customer;

import com.rain.common.base.page.BasePageDto;

/**
 * 客户对账单记录搜索DTO
 */
public class CardRecordSearchDto extends BasePageDto {

    private static final long serialVersionUID = -3701764680443704400L;
    /**
     * 客户ID
     */
    private String customerId;
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String mobile;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
