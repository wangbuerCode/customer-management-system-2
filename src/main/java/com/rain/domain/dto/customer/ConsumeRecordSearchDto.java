package com.rain.domain.dto.customer;

import com.rain.common.base.page.BasePageDto;

/**
 * 客户消费记录搜索DTO
 */
public class ConsumeRecordSearchDto extends BasePageDto {

    private static final long serialVersionUID = -3701764680443704400L;
    /**
     * 名称
     */
    private String name;
    /**
     * 电话
     */
    private String mobile;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

}
