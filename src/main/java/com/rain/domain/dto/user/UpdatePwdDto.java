package com.rain.domain.dto.user;

import java.io.Serializable;

/**
 * 修改密码DTO
 */
public class UpdatePwdDto implements Serializable {
    private static final long serialVersionUID = 7303119499451836870L;
    /**
     * 新密码
     */
    private String newPwd;

    /**
     * 重复新密码
     */
    private String reNewPwd;

    public String getNewPwd() {
        return newPwd;
    }

    public void setNewPwd(String newPwd) {
        this.newPwd = newPwd;
    }

    public String getReNewPwd() {
        return reNewPwd;
    }

    public void setReNewPwd(String reNewPwd) {
        this.reNewPwd = reNewPwd;
    }
}
