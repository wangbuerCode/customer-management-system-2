package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;
import com.rain.common.enums.BusinessTypeEnum;

import java.math.BigDecimal;

/**
 * 卡记录
 */
@TableName("t_card_record")
public class CardRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 会员ID
     */
    private String customerId;

    /**
     * 卡ID
     */
    private String cardId;

    /**
     * 业务ID
     */
    private String businessId;

    /**
     * 业务类型：充值、消费
     */
    private BusinessTypeEnum businessType;

    /**
     * 变化数
     */
    private BigDecimal changeNum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 卡余额
     */
    private BigDecimal money;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public BusinessTypeEnum getBusinessType() {
        return businessType;
    }

    public void setBusinessType(BusinessTypeEnum businessType) {
        this.businessType = businessType;
    }

    public BigDecimal getChangeNum() {
        return changeNum;
    }

    public void setChangeNum(BigDecimal changeNum) {
        this.changeNum = changeNum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
