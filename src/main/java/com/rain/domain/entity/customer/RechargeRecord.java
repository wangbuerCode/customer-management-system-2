package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * 充值记录
 */
@TableName("t_recharge_record")
public class RechargeRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 会员ID
     */
    private String customerId;

    /**
     * 卡ID
     */
    private String cardId;

    /**
     * 实收金额
     */
    private BigDecimal realMoney;

    /**
     * 优惠金额
     */
    private BigDecimal couponMoney;

    /**
     * 充值金额
     */
    private BigDecimal rechargeMoney;

    /**
     * 卡余额
     */
    private BigDecimal money;

    /**
     * 备注
     */
    private String remark;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public BigDecimal getRealMoney() {
        return realMoney;
    }

    public void setRealMoney(BigDecimal realMoney) {
        this.realMoney = realMoney;
    }

    public BigDecimal getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(BigDecimal couponMoney) {
        this.couponMoney = couponMoney;
    }

    public BigDecimal getRechargeMoney() {
        return rechargeMoney;
    }

    public void setRechargeMoney(BigDecimal rechargeMoney) {
        this.rechargeMoney = rechargeMoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
