package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;
import com.rain.common.enums.SexEnum;

/**
 * 客户
 */
@TableName("t_customer")
public class Customer extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 性别
     */
    private SexEnum sex;
    /**
     * 类型ID
     */
    private String customerTypeId;
    /**
     * 电话
     */
    private String mobile;

    /**
     * 地址
     */
    private String address;

    /**
     * 备注
     */
    private String remark;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public String getMobile() {
        return mobile;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
