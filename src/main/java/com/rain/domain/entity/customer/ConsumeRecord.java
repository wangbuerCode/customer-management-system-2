package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * 消费记录
 */
@TableName("t_consume_record")
public class ConsumeRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 会员ID
     */
    private String customerId;

    /**
     * 卡ID
     */
    private String cardId;

    /**
     * 消费金额
     */
    private BigDecimal consumeMoney;

    /**
     * 备注
     */
    private String remark;

    /**
     * 卡余额
     */
    private BigDecimal money;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public BigDecimal getConsumeMoney() {
        return consumeMoney;
    }

    public void setConsumeMoney(BigDecimal consumeMoney) {
        this.consumeMoney = consumeMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
