package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;

/**
 * 客户类型
 */
@TableName("t_customer_type")
public class CustomerType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类型名称
     */
    private String name;


    public String getName() {
        return name;
    }

    public CustomerType setName(String name) {
        this.name = name;
        return this;
    }
}
