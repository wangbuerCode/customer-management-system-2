package com.rain.domain.entity.customer;

import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;

import java.math.BigDecimal;

/**
 * 卡
 */
@TableName("t_card")
public class Card extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 客户ID
     */
    private String customerId;

    /**
     * 卡号
     */
    private String cardNo;

    /**
     * 卡金额
     */
    private BigDecimal money;

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 备注
     */
    private String remark;


    public String getCustomerId() {
        return customerId;
    }

    public Card setCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }
    public String getCardNo() {
        return cardNo;
    }

    public Card setCardNo(String cardNo) {
        this.cardNo = cardNo;
        return this;
    }
    public BigDecimal getMoney() {
        return money;
    }

    public Card setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }
    public Integer getVersion() {
        return version;
    }

    public Card setVersion(Integer version) {
        this.version = version;
        return this;
    }
    public String getRemark() {
        return remark;
    }

    public Card setRemark(String remark) {
        this.remark = remark;
        return this;
    }
}
