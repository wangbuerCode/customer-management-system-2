package com.rain.domain.entity.user;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.rain.common.base.BaseEntity;
import com.rain.common.enums.YNEnum;

/**
 * 用户表
 */
@TableName("t_user")
public class User extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @TableField("user_name")
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 是否可用Y可用N禁用
     */
    @TableField("is_enable")
    private YNEnum isEnable;

    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public YNEnum getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(YNEnum isEnable) {
        this.isEnable = isEnable;
    }
}

