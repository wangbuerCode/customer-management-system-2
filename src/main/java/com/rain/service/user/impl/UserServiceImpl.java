package com.rain.service.user.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.enums.YNEnum;
import com.rain.dao.user.UserMapper;
import com.rain.domain.dto.user.LoginDto;
import com.rain.domain.dto.user.UpdatePwdDto;
import com.rain.domain.entity.user.User;
import com.rain.domain.vo.user.LoginVo;
import com.rain.exception.BusinessException;
import com.rain.exception.Exceptions;
import com.rain.service.user.IUserService;
import com.rain.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * 服务实现类
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    /**
     * 用户登录
     *
     * @param loginDto
     * @return
     */
    @Override
    public LoginVo login(LoginDto loginDto) {
        //基础检验
        if (ObjectUtils.isEmpty(loginDto) || ObjectUtils.isEmpty(loginDto.getUserName())) {
            throw new BusinessException(Exceptions.User.USERNAME_NULL);
        }
        if (ObjectUtils.isEmpty(loginDto.getPassword())) {
            throw new BusinessException(Exceptions.User.PWD_NULL);
        }
        //通过用户名查询用户信息
        User user = getUserByUserName(loginDto.getUserName());
        if (ObjectUtils.isEmpty(user)) {
            throw new BusinessException(Exceptions.User.USER_NOT_EXIST);
        }
        if (!YNEnum.Y.equals(user.getIsEnable())) {
            throw new BusinessException(Exceptions.User.USER_DISABLED);
        }
        if (!Md5Util.MD5Encode(loginDto.getPassword().concat(user.getSalt())).equals(user.getPassword())) {
            throw new BusinessException(Exceptions.User.USERNAME_PWD_ERROR);
        }
        //清除用户密码
        user.setPassword(null);
        LoginVo loginVo = new LoginVo();
        loginVo.setUser(user);
        return loginVo;
    }

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    @Override
    public User getUserByUserName(String userName) {
        if (ObjectUtils.isEmpty(userName)) {
            return null;
        }
        User param = new User();
        param.setUserName(userName);
        return super.getOne(new QueryWrapper<>(param));
    }

    /**
     * 修改密码
     *
     * @param userId
     * @param updatePwdDto
     */
    @Override
    public void updatePwd(String userId, UpdatePwdDto updatePwdDto) {
        //校验
        if(ObjectUtils.isEmpty(userId)){
            throw new BusinessException(Exceptions.User.USER_ID_NULL);
        }
        if(ObjectUtils.isEmpty(updatePwdDto.getNewPwd())){
            throw new BusinessException(Exceptions.User.NEW_PWD_NOT_NULL);
        }
        if(ObjectUtils.isEmpty(updatePwdDto.getReNewPwd())){
            throw new BusinessException(Exceptions.User.RE_NEW_PWD_NOT_NULL);
        }
        if(!updatePwdDto.getNewPwd().equals(updatePwdDto.getReNewPwd())){
            throw new BusinessException(Exceptions.User.TWO_PWD_NOT_EQUAL);
        }

        String salt = RandomUtils.randomAlphanumeric(6);
        User userParam = new User();
        userParam.setId(userId);
        userParam.setSalt(salt);
        userParam.setPassword(Md5Util.MD5Encode(updatePwdDto.getNewPwd().concat(salt)));
        EntityUtils.init(userId, userParam);
        boolean flag = super.updateById(userParam);
        if (!flag) {
            LOGGER.error("修改密码失败，用户信息:{}", JsonUtils.toJson(userParam));
            throw new BusinessException(Exceptions.System.UPDATE_FAIL);
        }
    }
}
