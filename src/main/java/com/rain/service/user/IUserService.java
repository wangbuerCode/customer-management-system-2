package com.rain.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rain.domain.dto.user.LoginDto;
import com.rain.domain.dto.user.UpdatePwdDto;
import com.rain.domain.entity.user.User;
import com.rain.domain.vo.user.LoginVo;

/**
 * 服务类
 */
public interface IUserService extends IService<User> {
    /**
     * 登录
     *
     * @param loginDto
     * @return
     */
    LoginVo login(LoginDto loginDto);

    /**
     * 根据用户名查询用户信息
     *
     * @param userName
     * @return
     */
    User getUserByUserName(String userName);

    /**
     * 修改密码
     *
     * @param userId
     * @param updatePwdDto
     */
    void updatePwd(String userId, UpdatePwdDto updatePwdDto);
}
