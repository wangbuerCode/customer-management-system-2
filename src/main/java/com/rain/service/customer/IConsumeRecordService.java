package com.rain.service.customer;

import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.ConsumeRecordSearchDto;
import com.rain.domain.entity.customer.ConsumeRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 消费记录 服务类
 */
public interface IConsumeRecordService extends IService<ConsumeRecord> {
    /**
     * 消费记录搜索
     *
     * @param searchDto
     * @return
     */
    PageVo search(ConsumeRecordSearchDto searchDto);
}
