package com.rain.service.customer;

import com.rain.domain.entity.customer.Card;
import com.baomidou.mybatisplus.extension.service.IService;
import com.rain.domain.entity.customer.Customer;

/**
 * 卡 服务类
 */
public interface ICardService extends IService<Card> {
    /**
     * 初始化卡信息
     *
     * @param customer
     */
    void initCardInfo(Customer customer);

    /**
     * 删除
     *
     * @param customerId
     */
    void delByCustomerId(String customerId);
}
