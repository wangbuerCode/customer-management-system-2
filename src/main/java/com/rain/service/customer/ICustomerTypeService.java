package com.rain.service.customer;

import com.rain.domain.entity.customer.CustomerType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 客户类型 服务类
 */
public interface ICustomerTypeService extends IService<CustomerType> {

    /**
     * 搜索
     *
     * @return
     */
    List<CustomerType> search();
}
