package com.rain.service.customer;

import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.CardRecordSearchDto;
import com.rain.domain.entity.customer.CardRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 卡记录 服务类
 */
public interface ICardRecordService extends IService<CardRecord> {

    /**
     * 对账单记录搜索
     *
     * @param searchDto
     * @return
     */
    PageVo search(CardRecordSearchDto searchDto);
}
