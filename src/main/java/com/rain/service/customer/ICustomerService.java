package com.rain.service.customer;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.CustomerSearchDto;
import com.rain.domain.entity.customer.ConsumeRecord;
import com.rain.domain.entity.customer.Customer;
import com.rain.domain.entity.customer.RechargeRecord;

/**
 * 客户 服务类
 */
public interface ICustomerService extends IService<Customer> {
    /**
     * 分页搜索
     *
     * @param searchDto
     * @return
     */
    PageVo search(CustomerSearchDto searchDto);

    /**
     * 添加或修改
     *
     * @param customer
     */
    void addOrUpdate(Customer customer);

    /**
     * 删除
     *
     * @param id
     */
    void del(String id);

    /**
     * 充值
     *
     * @param rechargeRecord
     */
    void recharge(RechargeRecord rechargeRecord);

    /**
     * 消费
     * @param consumeRecord
     */
    void consume(ConsumeRecord consumeRecord);
}
