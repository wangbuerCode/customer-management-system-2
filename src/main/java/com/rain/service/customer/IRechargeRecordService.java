package com.rain.service.customer;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.RechargeRecordSearchDto;
import com.rain.domain.entity.customer.RechargeRecord;

/**
 * 充值记录 服务类
 */
public interface IRechargeRecordService extends IService<RechargeRecord> {
    /**
     * 充值记录搜索
     *
     * @param searchDto
     * @return
     */
    PageVo search(RechargeRecordSearchDto searchDto);
}
