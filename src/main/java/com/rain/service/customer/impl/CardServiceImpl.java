package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.enums.YNEnum;
import com.rain.dao.customer.CardMapper;
import com.rain.domain.entity.customer.Card;
import com.rain.domain.entity.customer.Customer;
import com.rain.service.customer.ICardService;
import com.rain.util.EntityUtils;
import com.rain.util.ObjectUtils;
import com.rain.util.RandomUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * 卡 服务实现类
 */
@Service
public class CardServiceImpl extends ServiceImpl<CardMapper, Card> implements ICardService {
    /**
     * 初始化卡信息
     *
     * @param customer
     */
    @Override
    public void initCardInfo(Customer customer) {
        Card card = new Card();
        EntityUtils.init(card);
        card.setCustomerId(customer.getId());
        card.setCardNo(RandomUtils.getRandomNumber());
        card.setMoney(BigDecimal.ZERO);
        card.setVersion(0);
        super.save(card);
    }

    /**
     * 删除
     *
     * @param customerId
     */
    @Override
    public void delByCustomerId(String customerId) {
        Card param = new Card();
        param.setCustomerId(customerId);
        Card card = super.getOne(new QueryWrapper<>(param));
        if(ObjectUtils.isNotEmpty(card)){
            EntityUtils.init(card);
            card.setVersion(card.getVersion()+1);
            card.setIsValid(YNEnum.N);
            super.updateById(card);
        }
    }
}
