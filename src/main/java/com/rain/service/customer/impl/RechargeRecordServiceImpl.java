package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.base.page.PageVo;
import com.rain.dao.customer.RechargeRecordMapper;
import com.rain.domain.dto.customer.RechargeRecordSearchDto;
import com.rain.domain.entity.customer.RechargeRecord;
import com.rain.domain.vo.customer.RechargeRecordVo;
import com.rain.service.customer.IRechargeRecordService;
import org.springframework.stereotype.Service;

/**
 * 充值记录 服务实现类
 */
@Service
public class RechargeRecordServiceImpl extends ServiceImpl<RechargeRecordMapper, RechargeRecord> implements IRechargeRecordService {
    /**
     * 充值记录搜索
     *
     * @param searchDto
     * @return
     */
    @Override
    public PageVo search(RechargeRecordSearchDto searchDto) {
        Page<RechargeRecordVo> page = new Page<>(searchDto.getPageNo(), searchDto.getPageSize());
        page.setRecords(this.baseMapper.search(page, searchDto));
        return new PageVo(page);
    }
}
