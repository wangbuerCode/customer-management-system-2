package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.base.page.PageVo;
import com.rain.dao.customer.ConsumeRecordMapper;
import com.rain.domain.dto.customer.ConsumeRecordSearchDto;
import com.rain.domain.entity.customer.ConsumeRecord;
import com.rain.domain.vo.customer.ConsumeRecordVo;
import com.rain.service.customer.IConsumeRecordService;
import org.springframework.stereotype.Service;

/**
 * 消费记录 服务实现类
 */
@Service
public class ConsumeRecordServiceImpl extends ServiceImpl<ConsumeRecordMapper, ConsumeRecord> implements IConsumeRecordService {
    /**
     * 消费记录搜索
     *
     * @param searchDto
     * @return
     */
    @Override
    public PageVo search(ConsumeRecordSearchDto searchDto) {
        Page<ConsumeRecordVo> page = new Page<>(searchDto.getPageNo(), searchDto.getPageSize());
        page.setRecords(this.baseMapper.search(page, searchDto));
        return new PageVo(page);
    }
}
