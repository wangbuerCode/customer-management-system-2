package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.base.page.PageVo;
import com.rain.common.enums.BusinessTypeEnum;
import com.rain.dao.customer.CardRecordMapper;
import com.rain.domain.dto.customer.CardRecordSearchDto;
import com.rain.domain.entity.customer.CardRecord;
import com.rain.domain.entity.customer.RechargeRecord;
import com.rain.domain.vo.customer.CardRecordVo;
import com.rain.service.customer.ICardRecordService;
import com.rain.service.customer.IRechargeRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 卡记录 服务实现类
 */
@Service
public class CardRecordServiceImpl extends ServiceImpl<CardRecordMapper, CardRecord> implements ICardRecordService {

    @Autowired
    private IRechargeRecordService rechargeRecordService;

    /**
     * 对账单记录搜索
     *
     * @param searchDto
     * @return
     */
    @Override
    public PageVo search(CardRecordSearchDto searchDto) {
        Page<CardRecordVo> page = new Page<>(searchDto.getPageNo(), searchDto.getPageSize());
        page.setRecords(this.baseMapper.search(page, searchDto));
        for (CardRecordVo vo : page.getRecords()) {
            if (BusinessTypeEnum.RECHARGE.equals(vo.getBusinessType())) {
                RechargeRecord rechargeRecord = rechargeRecordService.getById(vo.getBusinessId());
                vo.setRealMoney(rechargeRecord.getRealMoney());
                vo.setCouponMoney(rechargeRecord.getCouponMoney());
            }
        }
        return new PageVo(page);
    }
}
