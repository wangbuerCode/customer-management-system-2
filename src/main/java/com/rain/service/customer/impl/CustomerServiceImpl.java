package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rain.common.base.page.PageVo;
import com.rain.common.enums.BusinessTypeEnum;
import com.rain.common.enums.YNEnum;
import com.rain.dao.customer.CustomerMapper;
import com.rain.domain.dto.customer.CustomerSearchDto;
import com.rain.domain.entity.customer.*;
import com.rain.domain.vo.customer.CustomerVo;
import com.rain.exception.BusinessException;
import com.rain.exception.Exceptions;
import com.rain.service.customer.*;
import com.rain.util.EntityUtils;
import com.rain.util.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 客户 服务实现类
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements ICustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Autowired
    private ICardService cardService;

    @Autowired
    private ICardRecordService cardRecordService;

    @Autowired
    private IRechargeRecordService rechargeRecordService;

    @Autowired
    private IConsumeRecordService consumeRecordService;

    /**
     * 分页搜索
     *
     * @param searchDto
     * @return
     */
    @Override
    public PageVo search(CustomerSearchDto searchDto) {
        Page<CustomerVo> page = new Page<>(searchDto.getPageNo(), searchDto.getPageSize());
        page.setRecords(this.baseMapper.search(page, searchDto));
        return new PageVo(page);
    }

    /**
     * 添加或修改
     *
     * @param customer
     */
    @Override
    @Transactional
    public void addOrUpdate(Customer customer) {
        //客户
        EntityUtils.init(customer);
        boolean isAdd = ObjectUtils.isEmpty(customer.getId());
        super.saveOrUpdate(customer);
        if (isAdd) {
            //初始化卡信息
            cardService.initCardInfo(customer);
        }
    }

    /**
     * 删除
     *
     * @param id
     */
    @Override
    public void del(String id) {
        if (ObjectUtils.isNotEmpty(id)) {
            Customer customer = super.getById(id);
            EntityUtils.init(customer);
            customer.setIsValid(YNEnum.N);
            super.updateById(customer);
            cardService.delByCustomerId(id);
        }
    }

    /**
     * 充值
     *
     * @param rechargeRecord
     */
    @Override
    @Transactional
    public void recharge(RechargeRecord rechargeRecord) {
        //卡信息
        Card card = cardService.getById(rechargeRecord.getCardId());

        Card param = new Card();
        param.setMoney(card.getMoney().add(rechargeRecord.getRechargeMoney()));
        param.setId(card.getId());
        param.setVersion(card.getVersion() + 1);
        EntityUtils.init(param);

        Card where = new Card();
        where.setId(card.getId());
        where.setVersion(card.getVersion());

        boolean update = cardService.update(param, new UpdateWrapper<>(where));
        if (!update) {
            throw new BusinessException(Exceptions.System.SAVE_FAIL);
        }

        //充值记录
        EntityUtils.init(rechargeRecord);
        rechargeRecord.setMoney(param.getMoney());
        rechargeRecordService.save(rechargeRecord);

        //卡记录
        CardRecord cardRecord = new CardRecord();
        EntityUtils.init(cardRecord);
        cardRecord.setCardId(rechargeRecord.getCardId());
        cardRecord.setCustomerId(rechargeRecord.getCustomerId());
        cardRecord.setBusinessId(rechargeRecord.getId());
        cardRecord.setBusinessType(BusinessTypeEnum.RECHARGE);
        cardRecord.setChangeNum(rechargeRecord.getRechargeMoney());
        cardRecord.setMoney(param.getMoney());
        cardRecordService.save(cardRecord);
    }

    /**
     * 消费
     *
     * @param consumeRecord
     */
    @Override
    @Transactional
    public void consume(ConsumeRecord consumeRecord) {
        //卡信息
        Card card = cardService.getById(consumeRecord.getCardId());
        if (BigDecimal.ZERO.compareTo(card.getMoney()) >= 0) {
            throw new BusinessException(Exceptions.Customer.MONEY_NOT_ENOUGH);
        }
        Card param = new Card();
        param.setMoney(card.getMoney().subtract(consumeRecord.getConsumeMoney()));
        if (BigDecimal.ZERO.compareTo(param.getMoney()) > 0) {
            throw new BusinessException(Exceptions.Customer.MONEY_NOT_ENOUGH);
        }
        param.setId(card.getId());
        param.setVersion(card.getVersion() + 1);
        EntityUtils.init(param);

        Card where = new Card();
        where.setId(card.getId());
        where.setVersion(card.getVersion());

        boolean update = cardService.update(param, new UpdateWrapper<>(where));
        if (!update) {
            throw new BusinessException(Exceptions.System.SAVE_FAIL);
        }

        //消费记录
        EntityUtils.init(consumeRecord);
        consumeRecord.setMoney(param.getMoney());
        consumeRecordService.save(consumeRecord);

        //卡记录
        CardRecord cardRecord = new CardRecord();
        EntityUtils.init(cardRecord);
        cardRecord.setCardId(consumeRecord.getCardId());
        cardRecord.setCustomerId(consumeRecord.getCustomerId());
        cardRecord.setBusinessId(consumeRecord.getId());
        cardRecord.setBusinessType(BusinessTypeEnum.CONSUME);
        cardRecord.setChangeNum(consumeRecord.getConsumeMoney());
        cardRecord.setMoney(param.getMoney());
        cardRecordService.save(cardRecord);
    }
}
