package com.rain.service.customer.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.rain.common.enums.YNEnum;
import com.rain.domain.entity.customer.CustomerType;
import com.rain.dao.customer.CustomerTypeMapper;
import com.rain.service.customer.ICustomerTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 客户类型 服务实现类
 */
@Service
public class CustomerTypeServiceImpl extends ServiceImpl<CustomerTypeMapper, CustomerType> implements ICustomerTypeService {
    /**
     * 搜索
     *
     * @return
     */
    @Override
    public List<CustomerType> search() {
        CustomerType param = new CustomerType();
        param.setIsValid(YNEnum.Y);
        return super.list(new QueryWrapper<>(param));
    }
}
