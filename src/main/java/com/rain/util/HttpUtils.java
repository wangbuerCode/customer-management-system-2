package com.rain.util;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Http工具类
 */
public class HttpUtils {


    /**
     * 判断当前请求是ajax请求还是页面跳转
     */
    public static boolean isAjax(HttpServletRequest request) {
        String header = request.getHeader("X-Requested-With");
        return StringUtils.isNotBlank(header) && header.equals("XMLHttpRequest");
    }

    /**
     * 获取主机名称
     */
    public static String getHostname(HttpServletRequest request) {
        String scheme = request.getScheme();
        StringBuilder hostname = new StringBuilder(scheme);
        hostname.append("://").append(request.getServerName());
        int port = request.getServerPort();
        if ((scheme.equals("http") && port != 80) || (scheme.equals("https") && port != 443)) {
            hostname.append(':').append(port);
        }
        return hostname.toString();
    }

}
