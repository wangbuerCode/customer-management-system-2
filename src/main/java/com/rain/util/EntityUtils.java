package com.rain.util;


import com.rain.common.base.BaseEntity;
import com.rain.common.enums.YNEnum;
import com.rain.common.interceptor.UserContext;

import java.util.Date;

/**
 * 实体操作工具类
 */
public class EntityUtils {

    public static void init(BaseEntity entity) {
        init(UserContext.getUserId(), entity);
    }

    public static void init(String userId, BaseEntity entity) {
        Date now = new Date();
        userId = ObjectUtils.isEmpty(userId) ? "system" : userId;
        if (ObjectUtils.isEmpty(entity.getId())) {
            entity.setIsValid(YNEnum.Y);
            entity.setCreateBy(userId);
            entity.setCreateTime(now);
        }
        entity.setUpdateBy(userId);
        entity.setUpdateTime(now);
    }

}
