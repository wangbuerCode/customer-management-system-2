package com.rain.util;

import java.util.Collection;
import java.util.Map;

/**
 * 对象工具类
 */
public class ObjectUtils {
    /**
     * 验证一个对象是否非空，支持String,Map,Collection等。
     *
     * @param o
     * @return
     */
    public static boolean isEmpty(Object o) {
        if (o == null) {
            return true;
        }
        if (o instanceof String) {
            return ((String) o).length() == 0;
        }
        if (o instanceof Collection) {
            return ((Collection<? extends Object>) o).size() == 0;
        }
        if (o instanceof Map) {
            return ((Map<? extends Object, ? extends Object>) o).size() == 0;
        }
        if (o instanceof CharSequence) {
            return ((CharSequence) o).length() == 0;
        }
        if (o instanceof Object[]) {
            return ((Object[]) o).length == 0;
        }
        // These types would flood the log
        // Number covers: BigDecimal, BigInteger, Byte, Double, Float, Integer, Long, Short
        if (o instanceof Boolean) return false;
        if (o instanceof Number) return false;
        if (o instanceof Character) return false;
        if (o instanceof java.sql.Timestamp) return false;
        return false;
    }

    /**
     * 验证一个对象是否非空，支持String,Map,Collection等。
     *
     * @param o
     * @return
     */
    public static boolean isNotEmpty(Object o) {
        return !isEmpty(o);
    }
}