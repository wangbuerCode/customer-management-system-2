package com.rain.util;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 随机数工具类
 */
public class RandomUtils {

    /**
     * 获取指定范围内的随机数
     *
     * @param min 最小值
     * @param max 最大值
     * @return
     */
    public static int randomIntegerByMinAndMax(int min, int max) {
        if (min == 0) {
            max++;
        }
        Random random = new Random();
        return random.nextInt(max) % (max - min + 1) + min;
    }

    /**
     * 随机大小写字母+数字
     *
     * @param length 生成长度
     */
    public static String randomAlphanumeric(int length) {
        return RandomStringUtils.randomAlphanumeric(length);
    }

    /**
     * 获取UUID字符串， 去掉"-"并且大写
     */
    public static String randomUUIDToUpperCase() {
        String s = UUID.randomUUID().toString();
        s = s.replace("-", "");
        s = s.toUpperCase();
        return s;
    }

    /**
     * 生成16位随机数
     *
     * @return
     */
    public static String getRandomNumber() {
        /**
         * 生成规则
         * 日期(6位161122)+时间(时间毫秒5位)+5位随机数
         */
        int randomNum = (int) (Math.random() * 90000) + 10000;
        StringBuilder num = new StringBuilder();
        num.append(DateUtils.formatDate(new Date(), "yyMMdd")).append(DateUtils.getSecond()).append(randomNum);
        return num.toString();
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100000; i++) {
            System.out.println(randomIntegerByMinAndMax(8, 8));
            System.out.println(randomUUIDToUpperCase());
        }
    }
}
