package com.rain.util;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * 日期工具类
 */
public class DateUtils {
    public static final DateTimeFormatter datetimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

    public static final DateTimeFormatter date_sdf = DateTimeFormat.forPattern("yyyy-MM-dd");

    public static final DateTimeFormatter month_sdf = DateTimeFormat.forPattern("yyyy-MM");

    public static final DateTimeFormatter month_sdf_str = DateTimeFormat.forPattern("yyyy年MM月");

    public static final DateTimeFormatter date_sdf_yyyy_mm_dd = DateTimeFormat.forPattern("yyyy/MM/dd");

    public static final DateTimeFormatter time_sdf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");

    public static void main(String[] args) {
        //   DateTime today = new DateTime("2018-09-05 11:22:11");
        DateTime today = DateTime.parse("2018-02-05 11:22:11", datetimeFormat);
        Date d = today.toLocalDate().dayOfMonth().withMaximumValue().toDate();
        System.out.println(formatDate(d));
        System.out.println(today.toString("yyyy-MM-dd HH:mm"));
    }

    public static Date parseDate(String date) {
        return DateTime.parse(date, date_sdf).toDate();
    }

    /**
     * 根据指定的格式将字符串转换成Date 如输入：2003-11-19 11:20:20将按照这个转成时间
     *
     * @param src     将要转换的原始字符窜
     * @param pattern 转换的匹配格式
     * @return 如果转换成功则返回转换后的日期
     * @throws ParseException
     */
    public static Date parseDate(String src, DateTimeFormatter pattern) {
        return DateTime.parse(src, pattern).toDate();
    }

    /**
     * 获取下单时间【返回秒数5位，不足补零】
     */
    public static String getSecond() {
        String[] my = DateUtils.formatDate(new Date(), "HH:mm:ss").split(":");
        int hour = Integer.parseInt(my[0]);
        int min = Integer.parseInt(my[1]);
        int sec = Integer.parseInt(my[2]);
        long totalSec = hour * 3600 + min * 60 + sec;
        DecimalFormat df = new DecimalFormat("00000");
        String str2 = df.format(totalSec);
        return str2;
    }

    public static Date parseDateTime(String date) {
        return DateTime.parse(date, datetimeFormat).toDate();
    }

    public static Date parseTimeSDF(String date) {
        return DateTime.parse(date, time_sdf).toDate();
    }

    /**
     * 指定日期的默认显示，具体格式：年-月-日
     *
     * @param date 指定的日期
     * @return 指定日期按“年-月-日“格式显示
     */
    public static String formatDate(Date date) {
        return date_sdf.print(new DateTime(date));
    }

    /**
     * 默认日期按指定格式显示
     *
     * @param pattern 指定的格式
     * @return 默认日期按指定格式显示
     */
    public static String formatDate(Date date, String pattern) {
        return DateTimeFormat.forPattern(pattern).print(new DateTime(date));
    }

    /**
     * 指定日期的默认显示，具体格式：年-月
     *
     * @param date 指定的日期
     * @return 指定日期按“年-月“格式显示 2018-09
     */
    public static String formatMonth(Date date) {
        return month_sdf.print(new DateTime(date));
    }

    /**
     * 指定日期的默认显示，具体格式：年-月
     *
     * @param date 指定的日期
     * @return 指定日期按“年-月“格式显示 2018年09月
     */
    public static String formatMonthStr(Date date) {
        return month_sdf_str.print(new DateTime(date));
    }

    /**
     * 添加addMonth个月份
     *
     * @param date     传入时间
     * @param addMonth 添加月份数
     * @return
     */
    public static Date addMonth(Date date, Integer addMonth) {
        return new DateTime(date).plusMonths(addMonth).toDate();
    }

    /**
     * 计算两个月份之前的差值
     *
     * @param startMonth 2018-01
     * @param endMonth   2019-05
     * @return
     */
    public static int monthDiff(String startMonth, String endMonth) {
        String[] startMonthArr = startMonth.split("-");
        String[] endMonthArr = endMonth.split("-");
        return 12 * (Integer.parseInt(endMonthArr[0]) - Integer.parseInt(startMonthArr[0])) + Integer.parseInt(endMonthArr[1]) - Integer.parseInt(startMonthArr[1]);
    }
}