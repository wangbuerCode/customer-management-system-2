package com.rain.controller.customer;


import com.rain.common.base.BaseController;
import com.rain.common.base.JSONResult;
import com.rain.common.enums.YNEnum;
import com.rain.domain.entity.customer.CustomerType;
import com.rain.service.customer.ICustomerTypeService;
import com.rain.util.EntityUtils;
import com.rain.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 客户类型 前端控制器
 */
@Controller
@RequestMapping("/customerType")
public class CustomerTypeController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerTypeController.class);

    @Autowired
    private ICustomerTypeService customerTypeService;

    /**
     * 进入客户类型列表页面
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list() {
        LOGGER.debug("into /customerType/list >>>>>>>>>>>>>>>>>>>>>>>");
        return "customer/type_list";
    }

    /**
     * 客户类型
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult search() {
        LOGGER.debug("into /customerType/search >>>>>>>>>>>>>>>>>>>>>>>");
        List<CustomerType> typeList = customerTypeService.search();
        return new JSONResult(typeList);
    }

    /**
     * 添加或修改客户类型
     *
     * @return
     */
    @RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult add(CustomerType customerType) {
        LOGGER.debug("into /customerType/saveOrUpdate >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(customerType));
        EntityUtils.init(customerType);
        customerTypeService.saveOrUpdate(customerType);
        return new JSONResult();
    }

    /**
     * 删除
     *
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult del(String id) {
        LOGGER.debug("into /customerType/del >>>>>>>>>>>>>>>>>>>>>>>{}", id);
        CustomerType customerType = customerTypeService.getById(id);
        if (customerType != null) {
            EntityUtils.init(customerType);
            customerType.setIsValid(YNEnum.N);
            customerTypeService.updateById(customerType);
        }
        return new JSONResult();
    }
}
