package com.rain.controller.customer;


import com.rain.common.base.BaseController;
import com.rain.common.base.JSONResult;
import com.rain.common.base.page.PageVo;
import com.rain.common.enums.SexEnum;
import com.rain.domain.dto.customer.CustomerSearchDto;
import com.rain.domain.entity.customer.ConsumeRecord;
import com.rain.domain.entity.customer.Customer;
import com.rain.domain.entity.customer.RechargeRecord;
import com.rain.service.customer.ICustomerService;
import com.rain.service.customer.ICustomerTypeService;
import com.rain.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 客户 前端控制器
 */
@Controller
@RequestMapping("/customer")
public class CustomerController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private ICustomerService customerService;

    @Autowired
    private ICustomerTypeService customerTypeService;

    /**
     * 进入客户列表页面
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, Model model) {
        LOGGER.debug("into /customer/list >>>>>>>>>>>>>>>>>>>>>>>");
        model.addAttribute("sexEnum", SexEnum.values());
        model.addAttribute("typeList", customerTypeService.search());
        return "customer/list";
    }

    /**
     * 客户搜索
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult search(CustomerSearchDto searchDto) {
        LOGGER.debug("into /customer/search >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(searchDto));
        PageVo pageVo = customerService.search(searchDto);
        return new JSONResult(pageVo);
    }

    /**
     * 添加或修改
     *
     * @return
     */
    @RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult saveOrUpdate(Customer customer) {
        LOGGER.debug("into /customer/addOrUpdate >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(customer));
        customerService.addOrUpdate(customer);
        return new JSONResult();
    }

    /**
     * 充值
     *
     * @return
     */
    @RequestMapping(value = "/recharge", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult recharge(RechargeRecord rechargeRecord) {
        LOGGER.debug("into /customer/recharge >>>>>>>>>>>>>>>>>>>>>>>{}",JsonUtils.toJson(rechargeRecord));
        customerService.recharge(rechargeRecord);
        return new JSONResult();
    }

    /**
     * 消费
     *
     * @return
     */
    @RequestMapping(value = "/consume", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult consume(ConsumeRecord consumeRecord) {
        LOGGER.debug("into /customer/consume >>>>>>>>>>>>>>>>>>>>>>>{}",JsonUtils.toJson(consumeRecord));
        customerService.consume(consumeRecord);
        return new JSONResult();
    }
    /**
     * 删除
     *
     * @return
     */
    @RequestMapping(value = "/del", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult del(String id) {
        LOGGER.debug("into /customer/del >>>>>>>>>>>>>>>>>>>>>>>{}",id);
        customerService.del(id);
        return new JSONResult();
    }
}
