package com.rain.controller.customer;


import com.rain.common.base.BaseController;
import com.rain.common.base.JSONResult;
import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.ConsumeRecordSearchDto;
import com.rain.service.customer.IConsumeRecordService;
import com.rain.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 消费记录 前端控制器
 */
@Controller
@RequestMapping("/consumeRecord")
public class ConsumeRecordController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumeRecordController.class);

    @Autowired
    private IConsumeRecordService consumeRecordService;

    /**
     * 进入消费记录列表页面
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, Model model) {
        LOGGER.debug("into /consumeRecord/list >>>>>>>>>>>>>>>>>>>>>>>");
        return "customer/consume_record_list";
    }

    /**
     * 消费记录搜索
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult search(ConsumeRecordSearchDto searchDto) {
        LOGGER.debug("into /consumeRecord/search >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(searchDto));
        PageVo pageVo = consumeRecordService.search(searchDto);
        return new JSONResult(pageVo);
    }
}
