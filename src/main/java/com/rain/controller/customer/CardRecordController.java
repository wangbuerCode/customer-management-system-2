package com.rain.controller.customer;


import com.rain.common.base.BaseController;
import com.rain.common.base.JSONResult;
import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.CardRecordSearchDto;
import com.rain.service.customer.ICardRecordService;
import com.rain.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 卡记录 前端控制器
 */
@Controller
@RequestMapping("/cardRecord")
public class CardRecordController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsumeRecordController.class);

    @Autowired
    private ICardRecordService cardRecordService;

    /**
     * 对账单记录搜索
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult search(CardRecordSearchDto searchDto) {
        LOGGER.debug("into /cardRecord/search >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(searchDto));
        PageVo pageVo = cardRecordService.search(searchDto);
        return new JSONResult(pageVo);
    }
}
