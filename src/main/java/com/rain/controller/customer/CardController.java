package com.rain.controller.customer;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.rain.common.base.BaseController;

/**
 * 卡 前端控制器
 */
@RestController
@RequestMapping("/card")
public class CardController extends BaseController {

}
