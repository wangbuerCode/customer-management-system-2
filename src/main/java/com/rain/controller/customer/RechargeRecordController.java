package com.rain.controller.customer;


import com.rain.common.base.BaseController;
import com.rain.common.base.JSONResult;
import com.rain.common.base.page.PageVo;
import com.rain.domain.dto.customer.RechargeRecordSearchDto;
import com.rain.service.customer.IRechargeRecordService;
import com.rain.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 充值记录 前端控制器
 */
@Controller
@RequestMapping("/rechargeRecord")
public class RechargeRecordController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RechargeRecordController.class);

    @Autowired
    private IRechargeRecordService rechargeRecordService;

    /**
     * 进入充值记录列表页面
     *
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(HttpServletRequest request, Model model) {
        LOGGER.debug("into /rechargeRecord/list >>>>>>>>>>>>>>>>>>>>>>>");
        return "customer/recharge_record_list";
    }

    /**
     * 充值记录搜索
     *
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult search(RechargeRecordSearchDto searchDto) {
        LOGGER.debug("into /rechargeRecord/search >>>>>>>>>>>>>>>>>>>>>>>{}", JsonUtils.toJson(searchDto));
        PageVo pageVo = rechargeRecordService.search(searchDto);
        return new JSONResult(pageVo);
    }
}
