package com.rain.common.base;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.rain.common.enums.YNEnum;

import java.io.Serializable;
import java.util.Date;
/**
 * 基类
 */
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = -1246765389943451495L;

    @TableId
    private String id;
    /**
     * 创建者 - user_id
     */
    @TableField(value = "create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新人 - user_id
     */
    @TableField(value = "update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 是否有效，Y表示有效，N表示无效
     */
    @TableField(value = "is_valid")
    private YNEnum isValid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public YNEnum getIsValid() {
        return isValid;
    }

    public void setIsValid(YNEnum isValid) {
        this.isValid = isValid;
    }
}
