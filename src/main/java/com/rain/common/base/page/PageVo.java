package com.rain.common.base.page;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rain.util.ObjectUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页VO
 */
public class PageVo<T> implements Paging, Serializable {

    private static final long serialVersionUID = -45179336188314765L;

    /**
     * 当前页面
     */
    private long pageNo = 0;

    /**
     * 分页大小
     */
    private long pageSize = 0;

    /**
     * 总页数
     */
    private long totalPage = 0;

    /**
     * 总的记录数
     */
    private long totalCount = 0;

    /**
     * 分页列表
     */
    private List<T> list = new ArrayList<T>();

    public PageVo() {
    }

    public PageVo(Page page) {
        this.pageNo = page.getCurrent();
        this.pageSize = page.getSize();
        this.totalPage = page.getPages();
        this.totalCount = page.getTotal();
        this.list = page.getRecords();
    }

    @Override
    public long getPageNo() {
        return pageNo;
    }

    @Override
    public long getPageSize() {
        return pageSize;
    }

    @Override
    public long getTotalPage() {
        return totalPage;
    }

    @Override
    public long getTotalCount() {
        return totalCount;
    }

    @Override
    public List<T> getList() {
        return list;
    }

    @Override
    public long getLastPage() {
        return this.totalPage;
    }

    @Override
    public boolean isHasPreviousPage() {
        return pageNo > 1;
    }

    @Override
    public boolean isHasNextPage() {
        return this.pageNo < this.totalPage;
    }

    @Override
    public long getPreviousPage() {
        return pageNo - 1;
    }

    @Override
    public long getNextPage() {
        return this.pageNo + 1;
    }

    public void setList(List<T> list) {
        if (ObjectUtils.isNotEmpty(list)) {
            this.list = list;
        }
    }
}
