package com.rain.common.base.page;


import java.io.Serializable;

/**
 * 分页基础DTO
 */
public class BasePageDto implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8847682321728714433L;

    /**
     * 当前页面
     */
    private Integer pageNo = 1;

    /**
     * 分页大小
     */
    private Integer pageSize = 10;

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
