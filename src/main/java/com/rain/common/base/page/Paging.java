package com.rain.common.base.page;

import java.util.List;

/**
 * 自定义分页标签 - 分页接口
 */
public interface Paging<T> {

    long getPageNo();

    long getPageSize();

    long getTotalPage();

    long getTotalCount();

    List<T> getList();

    long getLastPage();

    boolean isHasPreviousPage();

    boolean isHasNextPage();

    long getPreviousPage();

    long getNextPage();
}
