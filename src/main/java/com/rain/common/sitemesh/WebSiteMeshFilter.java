package com.rain.common.sitemesh;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

public class WebSiteMeshFilter extends ConfigurableSiteMeshFilter {

    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        builder.addDecoratorPath("/*", "/WEB-INF/views/decorator.jsp")
                .addExcludedPath("/login")
                .addExcludedPath("/WEB-INF/views/decorator.jsp")
                .addExcludedPath("/upload");
        builder.addTagRuleBundles(new ExtHtmlTagRuleBundle());
    }
}