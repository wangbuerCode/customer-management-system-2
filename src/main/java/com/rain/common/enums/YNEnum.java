package com.rain.common.enums;


/**
 * 是否有效的枚举
 */
public enum YNEnum {

    Y("是"),
    N("否");

    private String name;

    YNEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return name();
    }
}
