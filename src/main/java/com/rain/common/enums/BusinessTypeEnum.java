package com.rain.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 业务类型：充值、消费
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum BusinessTypeEnum {
    RECHARGE("充值"),
    CONSUME("消费"),;
    private String name;

    BusinessTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return name();
    }
}
