package com.rain.common.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 性别
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum SexEnum {
    MALE("男"),
    FEMALE("女"),;
    private String name;

    SexEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return name();
    }
}
