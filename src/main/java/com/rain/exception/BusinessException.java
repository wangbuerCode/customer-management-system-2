package com.rain.exception;

/**
 * 业务异常
 */
public class BusinessException extends RuntimeException {
    /**
     * 异常状态码
     */
    private Integer code;

    /**
     * 异常描述
     */
    private String description;

    public BusinessException() {
        super();
    }

    public BusinessException(ExceptionType type) {
        super(type.getDescription());
        this.code = type.getCode();
        this.description = type.getDescription();
    }

    public Integer getCode() {
        return code;
    }


    public String getDescription() {
        return description;
    }
}
