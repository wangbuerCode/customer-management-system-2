package com.rain.exception;


import static com.rain.exception.Exceptions.Constant.*;

/**
 * 枚举异常常量
 */
public interface Exceptions {

    /**
     * 异常模块code
     */
    interface Constant {
        int SYSTEM = 900;// 系统异常
        int FILE = 950;// 文件
        int USER = 1000;// 用户
        int CUSTOMER = 2000;// 客户
    }

    enum System implements ExceptionType {
        SYSTEM_ERROR(SYSTEM + 1, "系统异常"),
        REQUEST_PARAM_ERROR(SYSTEM + 2, "请求参数错误"),
        REQUEST_METHOD_ERROR(SYSTEM + 3, "请求方式错误，请检查GET或者POST方式是否错误"),
        REQUEST_CONTENT_TYPE_ERROR(SYSTEM + 4, "请求方式错误，请检查Content-Type是否正确"),
        LOGIN_VALID(SYSTEM + 5, "登录失效"),
        ACCESS_TOKEN_NOT_ALLOW_NULL(SYSTEM + 6, "用户accessToken不能为空！"),
        ADD_FAIL(SYSTEM + 7, "添加失败"),
        UPDATE_FAIL(SYSTEM + 8, "修改失败"),
        SAVE_FAIL(SYSTEM + 9, "保存失败"),
        DEL_FAIL(SYSTEM + 10, "删除失败"),
        PLEASE_NOT_REPEAT_SUBMIT(SYSTEM + 11, "请勿重复提交"),
        ACCESS_TOO_FREQUENTLY(SYSTEM + 12, "访问太频繁！"),;

        private int code;
        private String description;

        System(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }

    enum User implements ExceptionType {
        USERNAME_NULL(USER + 1, "请输入用户名"),
        PWD_NULL(USER + 2, "请输入密码"),
        USERNAME_PWD_ERROR(USER + 3, "用户名或密码错误"),
        USER_NOT_EXIST(USER + 4, "用户不存在"),
        USER_DISABLED(USER + 5, "用户被禁用，请联系管理员"),
        USER_ID_NULL(USER + 6, "用户Id不能为空"),
        USERNAME_HAS_USED(USER + 7, "用户名已被使用，请重新输入"),
        NEW_PWD_NOT_NULL(USER + 8, "密码不能为空"),
        RE_NEW_PWD_NOT_NULL(USER + 9, "确认密码不能为空"),
        TWO_PWD_NOT_EQUAL(USER + 10, "两次输入的密码不一致，请重新输入"),;

        private int code;
        private String description;

        User(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }

    enum File implements ExceptionType {
        FILE_UPLOAD_NOT_EXIST(FILE + 1, "文件不存在，请重新上传"),
        FILE_UPLOAD_DIR_NOT_WRITE_PERMISSION(FILE + 2, "目录没有写权限，上传失败"),
        FILE_UPLOAD_TOO_BIG(FILE + 3, "文件过大，请重新上传"),
        FILE_UPLOAD_NOT_FIND_PATH(FILE + 4, "系统找不到指定的路径，上传失败"),
        FILE_UPLOAD_ERROR(FILE + 5, "文件上传错误"),;

        private int code;
        private String description;

        File(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }
    enum Customer implements ExceptionType {
        MONEY_NOT_ENOUGH(CUSTOMER + 1, "余额不足"),
        ;

        private int code;
        private String description;

        Customer(int code, String description) {
            this.code = code;
            this.description = description;
        }

        @Override
        public int getCode() {
            return code;
        }

        @Override
        public String getDescription() {
            return description;
        }
    }
}