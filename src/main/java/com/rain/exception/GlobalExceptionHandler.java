package com.rain.exception;

import com.rain.common.base.JSONResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 全局异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public static JSONResult exceptionHandler(Exception e, HttpServletResponse response, HttpServletRequest request) {
        e.printStackTrace();
        if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            LOGGER.debug("exception  code:[{}],message:[{}]", businessException.getCode(), businessException.getMessage());
            return new JSONResult(businessException.getCode(), businessException.getMessage());
        }
        if (e instanceof MissingServletRequestParameterException || e instanceof BindException) {
            LOGGER.debug("exception  code:[{}],message:[{}]", Exceptions.System.REQUEST_PARAM_ERROR.getCode(), e.getMessage());
            return new JSONResult(Exceptions.System.REQUEST_PARAM_ERROR.getCode(), Exceptions.System.REQUEST_PARAM_ERROR.getDescription());
        }
        if (e instanceof HttpRequestMethodNotSupportedException) {
            LOGGER.debug("exception  code:[{}],message:[{}]", Exceptions.System.REQUEST_METHOD_ERROR.getCode(), e.getMessage());
            return new JSONResult(Exceptions.System.REQUEST_METHOD_ERROR.getCode(), Exceptions.System.REQUEST_METHOD_ERROR.getDescription());
        }
        if (e instanceof HttpMediaTypeNotSupportedException) {
            LOGGER.debug("exception  code:[{}],message:[{}]", Exceptions.System.REQUEST_CONTENT_TYPE_ERROR.getCode(), e.getMessage());
            return new JSONResult(Exceptions.System.REQUEST_CONTENT_TYPE_ERROR.getCode(), Exceptions.System.REQUEST_CONTENT_TYPE_ERROR.getDescription());
        }
        if (e instanceof MultipartException) {
            LOGGER.debug("exception  code:[{}],message:[{}]", Exceptions.System.SYSTEM_ERROR.getCode(), "上传文件大小不能超过10M");
            return new JSONResult(Exceptions.System.SYSTEM_ERROR.getCode(), "上传文件大小不能超过10M");
        } else {
            LOGGER.debug("exception  code:[{}],message:[{}]", Exceptions.System.SYSTEM_ERROR.getCode(), e.getMessage());
            return new JSONResult(Exceptions.System.SYSTEM_ERROR.getCode(), Exceptions.System.SYSTEM_ERROR.getDescription());
        }
    }
}